import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DosScreen extends StatelessWidget {
  const DosScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String url = 'https://www.redenlace.com.bo/';
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Web Red Enlace'),
      ),
      body: WebViewWidget(
        controller: WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            print('Progreso de carga WebView (progress : $progress%)');
          },
          onNavigationRequest: (navigation) {
            if (navigation.url != url) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse(url)),
      ),
    );
  }
}








