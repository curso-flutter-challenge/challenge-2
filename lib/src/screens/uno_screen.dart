import 'package:flutter/material.dart';

class UnoScreen extends StatelessWidget {
  const UnoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Imagen'),
        backgroundColor: Colors.red,
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Image(
            image: NetworkImage(
                "https://www.redenlace.com.bo/images/blogs/es/r_19_beneficios-y-experiencia-de-aceptar-pagos-con-tarjeta-presente-1.jpg"
            ),
            width: 300,
          ),
          const SizedBox(height: 50),
          const Text("Imagen del blog de Red Enlace.")
        ],
      )),
    );
  }
}
